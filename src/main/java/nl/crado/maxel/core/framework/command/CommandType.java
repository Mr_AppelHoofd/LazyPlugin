package nl.crado.maxel.core.framework.command;

import nl.crado.maxel.core.framework.command.annotations.Command;
import nl.crado.maxel.core.framework.command.executor.FirstCommandExec;
import nl.crado.maxel.core.framework.command.executor.HybridCommandExec;
import nl.crado.maxel.core.framework.command.executor.SubCommandExec;

import org.bukkit.command.CommandExecutor;

public enum CommandType {

	SUBS {
		@Override
		public CommandExecutor getCommandExecutor(Class<?> clazz, Command cmd) {
			return new SubCommandExec(clazz, cmd);
		}
	}, SINGLE {
		@Override
		public CommandExecutor getCommandExecutor(Class<?> clazz, Command cmd) {
			return new FirstCommandExec(clazz, cmd);
		}
	}, HYBRID {
		@Override
		public CommandExecutor getCommandExecutor(Class<?> clazz, Command cmd) {
			return new HybridCommandExec(clazz, cmd);
		}
	};
	
	public abstract CommandExecutor getCommandExecutor(Class<?> clazz, Command cmd);
	
}
