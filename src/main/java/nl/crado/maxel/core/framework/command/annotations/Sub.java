package nl.crado.maxel.core.framework.command.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Sub {

	public String name(); /*permission will be formed using the upper permission and the sub name*/

	public boolean allowConsole() default false;

	public String usage() default "No usage specified.";
	
}
