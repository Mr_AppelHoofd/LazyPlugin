package nl.crado.maxel.core.framework;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import lombok.Getter;
import nl.crado.maxel.core.framework.annotations.DontRegister;
import nl.crado.maxel.core.framework.annotations.LoadFirst;
import nl.crado.maxel.core.framework.annotations.RepeatingTask;
import nl.crado.maxel.core.framework.annotations.SingleTask;
import nl.crado.maxel.core.framework.command.annotations.Command;
import nl.crado.maxel.core.framework.module.AbstractModule;
import nl.crado.maxel.core.framework.util.reflection.ReflectionUtil;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

@SuppressWarnings({ "unchecked", "deprecation" })
public class Maxel {

	private Optional<String> packageName = Optional.empty();
	@Getter private Optional<Plugin> plugin = Optional.empty();

	private Map<Class<?>, AbstractModule> modules = Collections.synchronizedMap(new HashMap<Class<?>, AbstractModule>());
	private Map<Class<?>, Listener> listeners = Collections.synchronizedMap(new HashMap<>());
	private Map<Class<?>, CommandExecutor> commands = Collections.synchronizedMap(new HashMap<>());
	private Map<Class<?>, Runnable> repeatingTasks = Collections.synchronizedMap(new HashMap<>());

	private Map<Class<?>, Runnable> singleTasks = Collections.synchronizedMap(new HashMap<>());

	@Getter private boolean loaded = false;

	private final SimpleCommandMap commandMap;

	/*TODO something like settings. example show startup console, show details*/

	public static final String LOG_PREFIX = " >>  ";
	private static String SPACES = LOG_PREFIX + "                 ";

	private static Maxel instance = new Maxel();
	private Maxel() {
		Optional<SimpleCommandMap> oscp = getCommandMap();
		if (oscp.isPresent()) {
			commandMap = oscp.get();
		}
		else {
			commandMap = null;
		}
	}
	public static Maxel getCore() {
		return instance;
	}

	private synchronized Optional<SimpleCommandMap> getCommandMap(){
		if (commandMap != null) {
			return Optional.of(commandMap);
		}
		try {
			Class<?> srv = Bukkit.getServer().getClass();
			Method getMap = srv.getDeclaredMethod("getCommandMap", (Class<?>[]) null);
			if (getMap != null) {
				Object o = getMap.invoke(Bukkit.getServer());
				if (o instanceof SimpleCommandMap) {
					return Optional.of((SimpleCommandMap) o);
				}
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

	public synchronized void enable() {
		if (loaded) return;
		if (!plugin.isPresent()) {
			System.out.println(LOG_PREFIX + "Unable to load modules!");
			System.out.println(LOG_PREFIX + "Reason: No Plugin specified!");
			return;
		}
		if (!packageName.isPresent()) {
			System.out.println(LOG_PREFIX + "Unable to load modules!");
			System.out.println(LOG_PREFIX + "Reason: No package specified!");
			return;
		}
		loadPriorityModules();
		loadModules();
		loadOthers();
		loaded = true;
	}
	public synchronized void disable() {
		//TODO
	}
	public Maxel setPackageName(String pn) {
		packageName = Optional.of(pn);
		return instance;
	}
	public Maxel setPlugin(Plugin p) {
		plugin = Optional.ofNullable(p);
		return instance;
	}
	public static <B extends AbstractModule> B getModule(final Class<B> clazz) {
		return (B) Maxel.getCore().modules.get(clazz);
	}
	public static <B extends Listener> B getListener(final Class<B> clazz) {
		return (B) Maxel.getCore().listeners.get(clazz);
	}
	public static <B extends CommandExecutor> B getCommand(final Class<B> clazz) {
		return (B) Maxel.getCore().commands.get(clazz);
	}
	public static <B extends Runnable> B getRepeatingTask(final Class<B> clazz) {
		return (B) Maxel.getCore().repeatingTasks.get(clazz);
	}
	public static <B extends Runnable> B getSingleTask(final Class<B> clazz) {
		return (B) Maxel.getCore().singleTasks.get(clazz);
	}
	public static <B extends Runnable> B runSingleTask(Class<B> clazz, boolean async) {
		Runnable run = getSingleTask(clazz);
		SingleTask st = ReflectionUtil.getAnnotation(clazz, SingleTask.class);
		if (st != null) {
			if (async) {
				Maxel.getCore().plugin.get().getServer().getScheduler().scheduleAsyncDelayedTask(Maxel.getCore().plugin.get(), run, st.delay());
			}
			else {
				Maxel.getCore().plugin.get().getServer().getScheduler().scheduleSyncDelayedTask(Maxel.getCore().plugin.get(), run, st.delay());
			}
		}
		return (B) run;
	}
	private synchronized void loadPriorityModules() {
		System.out.println(SPACES + "[PriorityModules]");
		try {
			ReflectionUtil.getClassesRecursively(packageName.get()).forEach(clazz -> {
				if (ReflectionUtil.implementsInterface(clazz, AbstractModule.class)) {
					LoadFirst lf = ReflectionUtil.getLoadFirstAnnotation(clazz);
					if (lf != null) {
						DontRegister dr = ReflectionUtil.getDontRegisterAnnotation(clazz);
						if (dr == null) {
							loadPriorityModule(clazz, lf);
						}
						else {
							System.out.println(LOG_PREFIX + "Not loading module: " + clazz.getSimpleName());
							System.out.println(LOG_PREFIX + "Reason: " + dr.reason());
						}
					}
				}
			});
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	private synchronized void loadPriorityModule(Class<?> clazz, LoadFirst lf) {
		System.out.println(LOG_PREFIX + "Priority loading for Module: " + clazz.getSimpleName());
		System.out.println(LOG_PREFIX + "Reason: " + lf.reason());
		loadModule(clazz);
	}
	private synchronized void loadModule(Class<?> clazz) {
		System.out.println(LOG_PREFIX + "Started loading module " + clazz.getSimpleName() + "...");
		try {
			AbstractModule a = (AbstractModule) clazz.newInstance();
			modules.put(clazz, a);
			System.out.println(LOG_PREFIX + "Enabling module!");
			a.enable();
			System.out.println(LOG_PREFIX + clazz.getSimpleName() + " has succesfully loaded!");
		} catch (Exception e) {
			System.out.println(LOG_PREFIX + "Something went wrong while loading " +  clazz.getSimpleName() + "!");
			e.printStackTrace();
		}
	}
	private void loadModules() {
		System.out.println(SPACES + "[Modules]");
		try {
			ReflectionUtil.getClassesRecursively(packageName.get()).forEach(clazz -> {
				if (ReflectionUtil.implementsInterface(clazz, AbstractModule.class)) {
					if (ReflectionUtil.getLoadFirstAnnotation(clazz) != null) {	/*Module is al geladen!*/
						return;
					}
					DontRegister dr = ReflectionUtil.getDontRegisterAnnotation(clazz);
					if (dr == null) {
						loadModule(clazz);
					}
					else {
						System.out.println(LOG_PREFIX + "Not loading module: " + clazz.getSimpleName());
						System.out.println(LOG_PREFIX + "Reason: " + dr.reason());
					}
				}
			});
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	private void loadOthers() {
		System.out.println(SPACES + "[Others]");
		try {
			ReflectionUtil.getClassesRecursively(packageName.get()).forEach(clazz -> {
				if (ReflectionUtil.isListener(clazz)) {
					DontRegister dr = ReflectionUtil.getDontRegisterAnnotation(clazz);
					if (dr == null) {
						try {
							loadListener(clazz);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else {
						System.out.println(LOG_PREFIX + "Not loading Listener: " + clazz.getSimpleName());
						System.out.println(LOG_PREFIX + "Reason: " + dr.reason());
					}
					return;
				}
				RepeatingTask rt = ReflectionUtil.getRepeatingTaskAnnotation(clazz);
				if (rt != null) {
					DontRegister dr = ReflectionUtil.getDontRegisterAnnotation(clazz);
					if (dr == null) {
						try {
							if (ReflectionUtil.implementsInterface(clazz, Runnable.class)) { 
								loadRepeatingTask(clazz, rt);
								return;
							}
							else if (ReflectionUtil.implementsInterface(clazz, BukkitRunnable.class)) {
								loadRepeatingTask(clazz, rt);
								return;
							}
							else {
								System.out.println(LOG_PREFIX + "Not loading RepeatingTask: " + clazz.getSimpleName());
								System.out.println(LOG_PREFIX + "Reason: Does not implements Runnable or BukkitRunnable!");
								return;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else {
						System.out.println(LOG_PREFIX + "Not loading RepeatingTask: " + clazz.getSimpleName());
						System.out.println(LOG_PREFIX + "Reason: " + dr.reason());
					}					
					return;
				}
				SingleTask st = ReflectionUtil.getSingleTaskAnnotation(clazz);
				if (st != null) {
					DontRegister dr = ReflectionUtil.getDontRegisterAnnotation(clazz);
					if (dr == null) {
						try {
							if (ReflectionUtil.implementsInterface(clazz, Runnable.class)) { 
								loadSingleTask(clazz, rt);
								return;
							}
							else if (ReflectionUtil.implementsInterface(clazz, BukkitRunnable.class)) {
								loadSingleTask(clazz, rt);
								return;
							}
							else {
								System.out.println(LOG_PREFIX + "Not loading SingleTask: " + clazz.getSimpleName());
								System.out.println(LOG_PREFIX + "Reason: Does not implements Runnable or BukkitRunnable!");
								return;
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else {
						System.out.println(LOG_PREFIX + "Not loading SingleTask: " + clazz.getSimpleName());
						System.out.println(LOG_PREFIX + "Reason: " + dr.reason());
					}					
					return;
				}
				Command cmd = ReflectionUtil.getAnnotation(clazz, Command.class);
				if (cmd != null) {
					DontRegister dr = ReflectionUtil.getDontRegisterAnnotation(clazz);
					if (dr == null) {
						try {
							loadCommand(clazz, cmd);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else {
						System.out.println(LOG_PREFIX + "Not loading Command: " + clazz.getSimpleName());
						System.out.println(LOG_PREFIX + "Reason: " + dr.reason());
					}					
					return;
				}
			});
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	private void loadListener(Class<?> clazz) throws InstantiationException, IllegalAccessException {
		System.out.println(LOG_PREFIX + "Started loading " + clazz.getSimpleName() + "...");
		System.out.println(LOG_PREFIX + "Attempting to make Listener instance...");
		Listener a = (Listener) clazz.newInstance();
		System.out.println(LOG_PREFIX + "Attempting to save listener instance...");
		listeners.put(clazz, a);
		System.out.println(LOG_PREFIX + "Attempting to register listener instance...");
		plugin.get().getServer().getPluginManager().registerEvents(a, plugin.get());
		System.out.println(LOG_PREFIX + clazz.getSimpleName() + " has succesfully loaded!");
	}
	private void loadRepeatingTask(Class<?> clazz, RepeatingTask rt) throws InstantiationException, IllegalAccessException {
		System.out.println(LOG_PREFIX + "Started loading " + clazz.getSimpleName() + "...");
		System.out.println(LOG_PREFIX + "Attempting to make task instance...");
		Runnable a = (Runnable) clazz.newInstance();
		System.out.println(LOG_PREFIX + "Attempting to register instance...");
		if (rt.async()) {
			plugin.get().getServer().getScheduler().scheduleAsyncRepeatingTask(plugin.get(), a, rt.delay(), rt.interval());
		}
		else {
			plugin.get().getServer().getScheduler().scheduleSyncRepeatingTask(plugin.get(), a, rt.delay(), rt.interval());
		}
		repeatingTasks.put(clazz, a);
		System.out.println(LOG_PREFIX + clazz.getSimpleName() + " has succesfully loaded!");
	}
	private void loadSingleTask(Class<?> clazz, RepeatingTask rt) throws InstantiationException, IllegalAccessException {
		System.out.println(LOG_PREFIX + "Started loading " + clazz.getSimpleName() + "...");
		System.out.println(LOG_PREFIX + "Attempting to make task instance...");
		Runnable a = (Runnable) clazz.newInstance();
		singleTasks.put(clazz, a);
		System.out.println(LOG_PREFIX + clazz.getSimpleName() + " has succesfully loaded!");
	}
	private void loadCommand(Class<?> clazz, Command cmd) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		System.out.println(LOG_PREFIX + "Creating new PluginCommand!");
		Constructor<?> con = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
		con.setAccessible(true);
		PluginCommand pc = (PluginCommand) con.newInstance(cmd.name(), plugin.get());
		System.out.println(LOG_PREFIX + "Setting PluginCommand settings!");
		pc.setAliases(Arrays.asList(cmd.aliases()));
		pc.setLabel(cmd.name());
		pc.setUsage(cmd.description());
		pc.setDescription(cmd.description());
		pc.setPermission(cmd.permission());
		pc.setPermissionMessage(cmd.permissionMessage());
		pc.setExecutor(cmd.type().getCommandExecutor(clazz, cmd));
		commands.put(clazz, pc.getExecutor());
		commandMap.register(plugin.get().getName(), pc);
		System.out.println(LOG_PREFIX + "Command " + clazz.getSimpleName() + " has succesfully loaded!");
	}
}
