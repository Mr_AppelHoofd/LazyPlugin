package nl.crado.maxel.core.framework.module;

import java.util.logging.Logger;

import nl.crado.maxel.core.framework.Maxel;

import org.bukkit.Server;
import org.bukkit.event.Event;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;

public interface AbstractModule {

	public default void enable() throws Exception {
		onEnable();
	}

	public abstract void onEnable() throws Exception;

	public default Plugin getPlugin() {
		return Maxel.getCore().getPlugin().get();
	}

	public default Server getServer() {
		return getPlugin().getServer();
	}
	
	public default PluginManager getPluginManager() {
		return getServer().getPluginManager();
	}
	
	public default BukkitScheduler getScheduler() {
		return getServer().getScheduler();
	}
	
	public default <T extends Event> T callEvent(T event) {
		getPluginManager().callEvent(event);
		return event;
	}
	
	public default Logger getLogger() {
		return getPlugin().getLogger();
	}
	
	public default void throwException(Exception e) {
		try {
			throw e;
		} catch (Exception e1) {
		}
	}
}
