package nl.crado.maxel.core.framework.datamanager;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unchecked")
public class DataManager {
	
	private Map<String, Map<Object, Object>> data;
	
	private static final DataManager instance = new DataManager();
	private DataManager() {
		data = new ConcurrentHashMap<>();
	}
	
	public static DataManager get() {
		return instance;
	}
	
	public synchronized Map<Object, Object> getMapByString(String name) {
		if (containsName(name)) {
			return data.get(name);
		}
		Map<Object, Object> map = new ConcurrentHashMap<>();
		data.put(name, map);
		return map;
	}
	
	public synchronized void removeAll(Object key) {
		data.values().forEach(m -> {
			if (m.containsKey(key)) {
				m.remove(key);
			}
		});
	}
	
	private synchronized boolean containsName(String name) {
		return data.containsKey(name);
	}
	
	public synchronized void setMap(String name, Map<Object, Object> map) {
		data.putIfAbsent(name, map);
	}
	
	public synchronized Optional<Object> getValue(String name, Object key) {
		if (containsName(name)) {
			return Optional.ofNullable(data.get(name).get(key));
		}
		return Optional.empty();
	}
	
	public synchronized <T extends Object> Optional<T> getValue(String name, Object key, final Class<T> clazz) {
		if (containsName(name)) {
			return Optional.ofNullable((T) data.get(name).get(key));
		}
		return Optional.empty();
	}
	
	public synchronized <T extends Object> T addValue(String name, Object key, T value) {
		getMapByString(name).put(key, value);
		return value;
	}
	
	
	
	public void foo() {
		
		Optional<Double> o = getValue("", "", Double.class);
		
		
	}
	
	
}
