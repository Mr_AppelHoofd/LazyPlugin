package nl.crado.maxel.core.framework.command.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bukkit.ChatColor;

import nl.crado.maxel.core.framework.command.CommandType;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Command {
	
	public String name();
	
	public ChatColor messageColor() default ChatColor.AQUA;
	
	public ChatColor variableColor() default ChatColor.DARK_AQUA;
	
	public String[] aliases() default {};
	
	public String description() default "No description specified.";
	
	public String permission();
	
	public String permissionMessage() default "You don't have the permission to do this!";
	
	public boolean allowConsole() default false;
	
	public CommandType type() default CommandType.SUBS;
	
	
	
	
	
}
