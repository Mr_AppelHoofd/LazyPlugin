package nl.crado.maxel.core.framework.command.info;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import nl.crado.maxel.core.framework.command.annotations.Command;
import nl.crado.maxel.core.framework.command.annotations.Sub;
import nl.crado.maxel.core.framework.command.exception.CommandException;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.google.common.base.Optional;

@RequiredArgsConstructor
public class CommandInfo implements ICommandInfo {

	@Getter private final CommandSender sender;
	private final String[] RawArgs;
	@Getter private final Optional<Sub> sub;
	@Getter private final Command command;

	@Override
	public String getSubPermission() {

		if (sub.isPresent()) {
			return command.permission() + "." + sub.get().name();
		}
		return command.permission();
	}

	@Override
	public void checkBoolean(boolean b, String message) {
		if (!b) {
			reply(message);
			throw new CommandException(message);
		}
	}

	@Override
	public boolean isNumber(String in) {
		try {
			int i = Integer.parseInt(in);
			return true;
		} catch(NumberFormatException e) {
			return false;
		}
	}
	
	@Override
	public void reply(String msg) {
		sender.sendMessage(command.messageColor() + msg);
	}

	@Override
	public void reply(String msg, Object... o) {
		String t = msg;
		for (int x = 0; x < o.length; x++) {
			t = t.replace("{" + x + "}", command.variableColor() + o[x].toString() + command.messageColor());
		}
		sender.sendMessage(command.messageColor() + t);
	}

	@Override
	public boolean checkPermission(String perm) {
		if (sender.hasPermission(perm)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public Optional<Player> getPlayer() {
		if (sender instanceof Player) {
			return Optional.of((Player) sender);
		}
		return Optional.absent();
	}

	@Override
	public Optional<Inventory> getInventory() {
		Optional<Player> p = getPlayer();

		if (p.isPresent()) {
			return Optional.of(p.get().getInventory());
		}
		return Optional.absent();
	}

	@Override
	public String[] getRawArgs() {
		return RawArgs;
	}

	@Override
	public String getArg(int i) {
		if (hasArg(i)) {
			return RawArgs[(i + 1)];
		}
		return null;
	}

	@Override
	public boolean hasArg(int i) {
		if (RawArgs.length > (i + 1)) {
			return true;
		}
		return false;
	}

}
