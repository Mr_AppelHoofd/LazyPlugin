package nl.crado.maxel.core.framework.command.exception;

public class CommandException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public CommandException(String msg) {
		super(msg);
	}
	
	@Override
	public void printStackTrace() {
		
	}
}
