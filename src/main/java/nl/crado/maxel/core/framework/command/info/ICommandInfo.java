package nl.crado.maxel.core.framework.command.info;

import nl.crado.maxel.core.framework.command.annotations.Command;
import nl.crado.maxel.core.framework.command.annotations.Sub;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import com.google.common.base.Optional;

public abstract interface ICommandInfo {

	Optional<Sub> getSub();
	
	Command getCommand();
	
	CommandSender getSender();
	
	Optional<Player> getPlayer();
	
	Optional<Inventory> getInventory();
	
	void checkBoolean(boolean b, String message);
	
	boolean isNumber(String in);
	
	void reply(String s);
	
	void reply(String s, Object... o);
	
	String[] getRawArgs();
	
	String getArg(int i);
	
	boolean hasArg(int i);
	
	boolean checkPermission(String perm);
	
	String getSubPermission();
}
