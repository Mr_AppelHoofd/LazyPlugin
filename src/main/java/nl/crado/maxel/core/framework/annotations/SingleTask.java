package nl.crado.maxel.core.framework.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface SingleTask {

	public String name();
	
	public String description() default "No description specified.";
	
	public long delay();
	
}
