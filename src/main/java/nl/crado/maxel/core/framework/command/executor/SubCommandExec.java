package nl.crado.maxel.core.framework.command.executor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import nl.crado.maxel.core.framework.Maxel;
import nl.crado.maxel.core.framework.command.annotations.Command;
import nl.crado.maxel.core.framework.command.annotations.Sub;
import nl.crado.maxel.core.framework.command.info.CommandInfo;
import nl.crado.maxel.core.framework.command.info.ICommandInfo;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

import com.google.common.base.Optional;

public class SubCommandExec implements CommandExecutor {

	private final Command cmd;
	private final Class<?> clazz;
	private Optional<Object> obj = Optional.absent();
	private final Map<String, Method> methods = Collections.synchronizedMap(new HashMap<>());

	public SubCommandExec(Class<?> c, Command cm) {
		clazz = c;
		cmd = cm;
		try {
			obj = Optional.of(clazz.newInstance());
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println(Maxel.LOG_PREFIX + "Command name: " + cmd.name());
		System.out.println(Maxel.LOG_PREFIX + "Type: " + cmd.type().toString());
		System.out.println(Maxel.LOG_PREFIX + "Allow console: " + cmd.allowConsole());
		System.out.println(Maxel.LOG_PREFIX + "Description: " + cmd.description());
		System.out.println(Maxel.LOG_PREFIX + "Permission: " + cmd.permission());
		System.out.println(Maxel.LOG_PREFIX + "Permission message: " + cmd.permissionMessage());
		for (Method m : clazz.getMethods()) {
			Sub sub = m.getAnnotation(Sub.class);
			if (sub != null) {
				methods.put(sub.name(), m);
				System.out.println(Maxel.LOG_PREFIX + "Sub name: " + sub.name());
				System.out.println(Maxel.LOG_PREFIX + "Usage: " + sub.usage());
				System.out.println(Maxel.LOG_PREFIX + "Allow console: " + sub.allowConsole());
			}
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
		if (sender instanceof ConsoleCommandSender && (!cmd.allowConsole())) {
			sender.sendMessage(ChatColor.RED + "This command is only available for players!");
			return true;
		}
		String[] rawArgs = args.clone();
		if (args.length == 0) {
			sender.sendMessage(cmd.messageColor() + "[=]-----+-----[=]");
			if (methods.isEmpty()) {
				sender.sendMessage(cmd.messageColor() + "No known sub commands!");
			}
			else {
				for (String key : methods.keySet()) {
					sender.sendMessage(cmd.messageColor() + key + ": " + cmd.variableColor() + methods.get(key).getAnnotation(Sub.class).usage());
				}
			}
			sender.sendMessage(cmd.messageColor() + "[=]-----+-----[=]");
			return true;
		}
		Optional<Method> om = findMethod(rawArgs[0]);
		if (om.isPresent()) {
			Method m = om.get();
			Sub sub = m.getAnnotation(Sub.class);
			ICommandInfo cmdI = new CommandInfo(sender, rawArgs, Optional.of(sub), cmd);
			if (sender instanceof ConsoleCommandSender && (!sub.allowConsole())) {
				sender.sendMessage(ChatColor.RED + "This command is only available for players!");
				return true;
			}
			else {
				if (!sender.hasPermission(cmdI.getSubPermission())) {
					sender.sendMessage(ChatColor.RED + cmd.permissionMessage());
					return true;
				}
				if (obj.isPresent()) {
					try {
						m.invoke(obj.get(), cmdI);
						return true;
					} catch (InvocationTargetException e) {
						e.getCause().printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
				else {
					sender.sendMessage(ChatColor.RED + "Something went wrong while registering the command! Probably the constructor.");
				}
			}
		}
		else {
			sender.sendMessage(ChatColor.RED + "We are not familiar with this command!");
		}
		return true;
	}

	private Optional<Method> findMethod(String cmdName) {
		for (String key : methods.keySet()) {
			if (key.equalsIgnoreCase(cmdName)) {
				return Optional.of(methods.get(key));
			}
		}
		return Optional.absent();
	}
}
